from TorCrawler import TorCrawler
from fake_useragent import UserAgent
from exec_time_decorator import exec_time_decorator
# import time
# import requests
# import socks
# import socket
# from bs4 import BeautifulSoup


def check_ip():
    crawler = TorCrawler(ctrl_pass='pass')
    ip_page = 'https://2ip.ru/'
    response = None
    # response = requests.get(url=ip_page, headers={'User-Agent': UserAgent().chrome})
    for _ in range(3):
        try:
            response = crawler.get(ip_page, headers={'User-Agent': UserAgent().chrome})
            break
        except Exception as ex:
            print(ex)
            print('##########')
    print(response.find(id='d_clip_button').text.strip())
    print(crawler.ip)
    # print('--------------------------')
    # for _ in range(1):
    #     crawler.rotate()
    #     print(crawler.ip)


@exec_time_decorator
def main():
    # socks.set_default_proxy(socks.SOCKS5, "localhost", 9051)
    # socket.socket = socks.socksocket
    check_ip()


if __name__ == '__main__':
    main()

