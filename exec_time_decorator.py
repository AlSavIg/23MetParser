from datetime import datetime


def exec_time_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = datetime.now()
        result = func(*args, **kwargs)
        print('Execution time:', datetime.now() - start_time)
        return result

    return wrapper
