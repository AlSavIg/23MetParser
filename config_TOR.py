from fake_useragent import UserAgent


ua = UserAgent()

cookies = {
}

headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
              'application/signed-exchange;v=b3;q=0.9',
    'Accept-Language': 'ru,en;q=0.9',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
    'Origin': 'http://23met.ru',
    'Referer': 'http://23met.ru/price/tryba_es_kvadr',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': f'{ua.random}',
}

data = {
    'g-recaptcha-response': '03AIIukzhDDI5MoV9-Q93UTe6Rp8G7yiYeVjHJdPGzwHKX5gRRfyW8NC_'
                            '-qjbVr5oliePJS5qVGG5h9s_hmv_rSgs5YFGCznHJa43NlkvP33O8TXHpAV3LkC1J'
                            '6GxRvCdvfdpJF37kUtzx465wbXiEpQsLhdIlYBssrtFF2zIiSMnTklxnGyFII4Y_WF'
                            '8508ZX0iXDa13EbtKcK0qawIcOrqaC6LWlBewTeNRdhlKSLJt8KcWVKE_8Cz_ZeM9'
                            'NnghBKgBkKetxzI22-JBUyP7i04utYGkAyKIfvVG8gAj5SS3FmCaGNWZux7OqbiNom'
                            '8QtUKc0yuuv0ZU6abOrnKLCwvcGPGIewolOKU-zSWLtFJdSKjWHvhN8QwN167-uwub'
                            'MLtA3TQ-ysuvsJM-wzKsrq5isSkZF7YRj0k7jCPXfyMrJROw63gfXcpCoF9aEAk9-i'
                            '8i-y6pJYM2vDN2o5WN0T_uJSuc6QUPmOcrjakuTLCCpUaVqeEwkLjIpg4L1bGnrSvN03XFfU5jh',
}

url = 'http://23met.ru/price/polosa/2%D1%85100'
