import pprint
from bs4 import BeautifulSoup


def get_data_from_file(link):
    data = []
    with open('data_from_website.html', 'r') as read_file:
        soup = BeautifulSoup(read_file.read(), 'lxml')
        tables = soup.find_all('table')
        for table in tables:
            body = table.find('tbody')
            rows = body.find_all('tr')
            page = []
            for row in rows:
                if row.get('class') != ['tp-tr-hidden']:
                    cost1 = row.find('span', class_='cost')
                    cost2 = row.find('span', class_='cost2')
                    company_name = row.find('a', class_='firm_link')
                    cost1 = cost1.text.strip() if cost1 else None
                    cost2 = cost2.text.strip() if cost2 else None
                    company_name = company_name.text.strip() if company_name else None
                    page.append({
                        'cost1': cost1,
                        'cost2': cost2,
                        'company_name': company_name,
                    })
            page.append(link)
            data.append(page)
    return data


def get_data_from_stdin(table_soup, link):
    rows = table_soup.find('tbody').find_all('tr')
    page = []
    for row in rows:
        if row.get('class') != ['tp-tr-hidden']:
            cost1 = row.find('span', class_='cost')
            cost2 = row.find('span', class_='cost2')
            company_name = row.find('a', class_='firm_link')
            cost1 = cost1.text.strip() if cost1 else None
            cost2 = cost2.text.strip() if cost2 else None
            company_name = company_name.text.strip() if company_name else None
            page.append(
                {
                    'cost1': cost1,
                    'cost2': cost2,
                    'company_name': company_name,
                    'link': link,
                }
            )
    return page


def main():
    data = get_data_from_file("link")
    pprint.pprint(data)


if __name__ == '__main__':
    main()
