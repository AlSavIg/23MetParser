import pprint
import openpyxl
import re
import string


def read_urls_from_excel(sheet, last_row) -> list[str]:
    return [sheet.cell(row=row, column=1).value for row in range(2, last_row + 1)]


def get_local_data():
    wb = openpyxl.load_workbook(filename='sample.xlsx')
    sheet = wb['Export']
    last_row = int(re.search(r':\D+(\d+)', sheet.dimensions)[1])
    last_column = string.ascii_uppercase.find(re.search(r':(\D+)\d+', sheet.dimensions)[1]) + 1
    titles = [sheet.cell(row=1, column=col).value for col in range(1, last_column + 1)]
    return [
        {titles[col - 1]: sheet.cell(row=row, column=col).value for col in range(1, last_column + 1)}
        for row in range(2, last_row + 1)
    ]


def main():
    demo = get_local_data()
    print(len(demo))
    pprint.pprint(demo)


if __name__ == '__main__':
    main()
