import json
import random
import threading
import time
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from python_anticaptcha import AnticaptchaClient, NoCaptchaTaskProxylessTask
from TorCrawler import TorCrawler
from scrap_local_data import get_data_from_stdin

mutex = threading.Semaphore(3)


def solve(url, site_key):
    try:
        mutex.acquire()
        print('CAPTCHA is solving...')
        api_key = 'b732bde881f536230a7bd1b5658b7d9b'
        client = AnticaptchaClient(api_key)
        task = NoCaptchaTaskProxylessTask(url, site_key)
        job = client.createTask(task)
        job.join()
        key = job.get_solution_response()
        print('CAPTCHA solved!')
        return {
            'g-recaptcha-response': key
        }
    except Exception as ex:
        print(ex)
    finally:
        mutex.release()


def prepare_params(url, headers, ref):
    ua = UserAgent()
    url = url.replace('х', 'x').strip()
    safe_url = 'http://23met.ru/price/polosa/5%D1%85130#st1-3'
    if ref:
        headers.update([('User-Agent', ua.random), ('Referer', url)])
    else:
        headers.update([('User-Agent', ua.random), ('Referer', safe_url)])
    return url, headers


def read_url_list(start, stop):
    with open('urls.txt', 'r', encoding='utf-8') as read_file:
        return read_file.readlines()[start:stop]


def cr_rotate(crawler, sleep_sec):
    crawler.rotate()
    time.sleep(sleep_sec)


def log_print(cur_num, cost, is_tab, url, traceback):
    if traceback:
        try:
            print("--" + str(cur_num) + "--",
                  traceback.find('td').text.strip(),
                  url,
                  sep='\n')
        except AttributeError:
            print("--" + str(cur_num) + "--",
                  traceback,
                  url,
                  sep='\n')
    else:
        print("--" + str(cur_num) + "--",
              "Count of costs: " + str(len(cost)) + " " + str(is_tab),
              url,
              sep='\n')


def tor_data_access(start, stop, filename):
    from config_TOR import headers
    current_url_num = 0
    crawler_requests = 50
    # proxy being used together by all threads in the same time
    crawler = TorCrawler(ctrl_pass='pass',
                         n_requests=crawler_requests)
    # cr_rotate(crawler=crawler, sleep_sec=2)
    url_list = read_url_list(start, stop)
    _headers = headers.copy()

    with open(filename, 'a', encoding='utf-8') as write_file:
        write_file.write('{\n')
        while current_url_num < len(url_list):
            url, _headers = prepare_params(url=url_list[current_url_num], headers=_headers, ref=True)
            try:
                soup = crawler.get(url, headers=_headers)
            except UnicodeEncodeError:
                url, _headers = prepare_params(url=url_list[current_url_num], headers=_headers, ref=False)
                soup = crawler.get(url, headers=_headers)

            site_key = soup.find('div', class_='g-recaptcha')
            is_site_key = bool(site_key)

            if is_site_key:
                site_key = site_key.get('data-sitekey')
                data = solve(url=url, site_key=site_key)
                crawler.post(url, headers=headers, data=data)
                time.sleep(random.random() + 1)
                url, _headers = prepare_params(url=url_list[current_url_num], headers=_headers, ref=False)
                soup = crawler.get(url, headers=_headers)

            try:
                table = soup.find('table', id='table-price')
                is_table = bool(table)
            except AttributeError:
                is_table = False

            if is_table:
                elem = {str(current_url_num): get_data_from_stdin(table_soup=table, link=url)}
                elem = json.dumps(
                    elem,
                    ensure_ascii=False,
                    indent=4
                ).strip('{').strip('}').strip()
                write_file.write(f'{elem},\n')

            costs = soup.find_all('td', class_='td_cost') if is_table else []
            traceback = soup if not is_table and not is_site_key else None

            log_print(current_url_num + start, costs, is_table, url, traceback)

            if not is_table and not traceback:
                print('\n\n', soup, '\n\n')

            if isinstance(traceback, BeautifulSoup) and 'Можно продолжить' in str(soup):
                print('Current IP was banned')
                crawler.rotate()
                continue

            current_url_num += 1

            time.sleep(random.random() + 1.5)

        write_file.write('\n}')


def main():
    thread1 = threading.Thread(target=tor_data_access, args=(0, 5714, 'TOR_1.json'))
    thread2 = threading.Thread(target=tor_data_access, args=(5714, 11428, 'TOR_2.json'))
    thread3 = threading.Thread(target=tor_data_access, args=(11428, 17142, 'TOR_3.json'))
    thread4 = threading.Thread(target=tor_data_access, args=(17142, 22858, 'TOR_4.json'))

    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()


if __name__ == '__main__':
    main()
    # crawler1 = TorCrawler(ctrl_pass='pass')
    # crawler2 = TorCrawler(ctrl_pass='pass')
    # print('cr1 before:', crawler1.ip)
    # print('cr2 before:', crawler2.ip)
    # crawler1.rotate()
    # print('cr1 after:', crawler1.ip)
    # print('cr2 after:', crawler2.ip)
